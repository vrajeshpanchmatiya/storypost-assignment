import Axios from "axios";

export const Api_Post = (id) => {
  return Axios.get(`${process.env.REACT_APP_API_URL}${id}`);
};
