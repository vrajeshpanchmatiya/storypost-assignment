import { Button } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import ReactTable from "react-table-v6";
import "react-table-v6/react-table.css";
import { postAction } from "../Actions/postAction";
import { Link } from "react-router-dom";
const AddPost = () => {
  const [id, setId] = useState(0);
  const dispatch = useDispatch();
  useEffect(() => {
    setTimeout(() => {
      dispatch(postAction(id));
      setId(id + 1);
    }, 10000);
  }, [dispatch, id]);
  const detail = useSelector((state) => {
    return state.data;
  });
  const columns = [
    {
      Header: "TITLE",
      accessor: "title",
      style: { backgroundColor: "aliceblue" },
    },
    {
      Header: "URL",
      accessor: "url",
      style: { backgroundColor: "lightgreen" },
    },
    {
      Header: "CREATED_AT",
      accessor: "created_at",
      style: { backgroundColor: "yellow" },
    },
    {
      Header: "AUTHOR",
      accessor: "author",
      style: { backgroundColor: "salmon" },
    },
    {
      Header: "Action",
      style: { backgroundColor: "lightgrey" },
      Cell: (props) => {
        return (
          <Link
            to={{ pathname: "/OriginalPost", data: props.original }}
            style={{ textDecoration: "none" }}
            color="secondary"
          >
            <Button type="submit" variant="outlined" color="secondary">
              Fetch Row
            </Button>
          </Link>
        );
      },
    },
  ];
  return (
    <ReactTable
      columns={columns}
      noDataText="Please Wait for a Seconds"
      defaultPageSize={20}
      showPageSizeOptions={false}
      data={detail}
    />
  );
};
export default AddPost;
