import { postType } from "../Actions/Type/postType";
const initialState = {
  data: [],
};
export const postReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case postType:
      return {
        ...state,
        data: state.data.concat(payload),
      };
    default:
      return state;
  }
};
