import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import AddPost from "./Components/AddPost";
import OriginalPost from "./Components/OriginalPost";
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={AddPost} />
          <Route path="/OriginalPost" component={OriginalPost} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
