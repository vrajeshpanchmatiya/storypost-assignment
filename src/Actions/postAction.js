import { postType } from "./Type/postType";
import { Api_Post } from "../Services/Api_Post";
export const postAction = (id) => {
  return async (dispatch) => {
    const detail = await Api_Post(id);
    dispatch({ type: postType, payload: detail.data.hits });
  };
};
